# Waypoint action flytlink

## Introduction
Please refer to MainActivity.java file.
The code shows how to add waypoints on the map ,upload the misssion on the DJI drone and run the mission.

## Requirements

 - Android Studio 2.0+
 - Android System 4.1+
 - DJI Android SDK 4.7.1

## Tutorial

For this demo's tutorial: **Creating a MapView and Waypoint Application**, please refer to <https://developer.dji.com/mobile-sdk/documentation/android-tutorials/GSDemo-Google-Map.html>.

## Waypoint action code
The list of waypoints is defined from lines 150-189.
The function to add actions to individual waypoints is in the function onMapClick() from line 367-405.
